package pt.lunata.hitamado

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Sjtapp : Application()