package pt.lunata.hitamado.model.login

class LoginEntity(
    val token: String? = null,
    val uid: String? = null,
    val jabatan: String? = null
)