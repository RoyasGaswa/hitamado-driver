package pt.lunata.hitamado.model.siapantar

import com.google.gson.annotations.SerializedName

data class ResponseConfirmSiapantar(
    @field:SerializedName("success")
    val success: Boolean? = null
)