package pt.lunata.hitamado.model.driver.entity


data class JabatanEntity(
    val updatedAt: Any? = null,
    val jabatan: String? = null,
    val gaji: Int? = null,
    val createdAt: Any? = null,
    val id: Int? = null,
    val deletedAt: Any? = null
)