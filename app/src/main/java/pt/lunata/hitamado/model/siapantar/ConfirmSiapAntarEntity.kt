package pt.lunata.hitamado.model.siapantar

data class ConfirmSiapAntarEntity(
    val success: Boolean? = null
)