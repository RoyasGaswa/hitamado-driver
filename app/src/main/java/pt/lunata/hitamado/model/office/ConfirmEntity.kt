package pt.lunata.hitamado.model.office

data class ConfirmEntity(
    val success: Boolean? = null
)