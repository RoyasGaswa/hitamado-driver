package pt.lunata.hitamado.model.office

data class ContinueOfficeEntity(
    val success: Boolean? = null
)