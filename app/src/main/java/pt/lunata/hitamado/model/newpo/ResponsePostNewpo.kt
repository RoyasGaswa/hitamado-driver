package pt.lunata.hitamado.model.newpo

import com.google.gson.annotations.SerializedName

data class ResponsePostNewpo(

	@field:SerializedName("success")
	val success: Boolean? = null
)
