package pt.lunata.hitamado.model.office

import com.google.gson.annotations.SerializedName

data class ResponseConfirmOffice(
    @field:SerializedName("success")
    val success: Boolean? = null
)