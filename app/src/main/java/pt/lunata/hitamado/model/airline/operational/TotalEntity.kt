package pt.lunata.hitamado.model.airline.operational

data class TotalEntity(
    val total: Int?
)