package pt.lunata.hitamado.model.pickup

data class ConfirmEntity(
    val confirmed: Boolean? = null
)