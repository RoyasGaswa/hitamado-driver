package pt.lunata.hitamado.model.siapantar

data class ContinueSiapAntarEntity(
    val success: Boolean? = null
)