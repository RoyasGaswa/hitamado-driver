package pt.lunata.hitamado.model.driver.entity

data class DriverEntity(
    val jabatanAppend: JabatanEntity? = null,

    val noEmergency1: String? = null,

    val mulaiKerja: String? = null,

    val idAgama: Int? = null,

    val createdAt: String? = null,

    val noWa: String? = null,

    val nominalBpjsKetenagakerjaan: String? = null,

    val idOffice: String? = null,

    val nominalBpjsKesehatan: String? = null,

    val idJabatan: Int? = null,

    val uid: String? = null,

    val nik: String? = null,

    val password: String? = null,

    val nominalNpwp: String? = null,

    val idJk: Int? = null,

    val updatedAt: String? = null,

    val nominalMakan: String? = null,

    val idPegawai: Int? = null,

    val noNpwp: String? = null,

    val email: String? = null,

    val tanggalLahir: String? = null,

    val idDivisi: Int? = null,

    val gaji: Int? = null,

    val noBpjsKetenagakerjaan: String? = null,

    val ikatanNoEmergency1: String? = null,

    val deletedAt: Any? = null,

    val alamat: String? = null,

    val noKontak: String? = null,

    val nama: String? = null,

    val foto: String? = null,

    val userLevel: Int? = null,

    val nominalTransport: String? = null,

    val noBpjsKesehatan: String? = null,

    val uidAtasan: String? = null
)