package pt.lunata.hitamado.model.office

import com.google.gson.annotations.SerializedName

data class ResponseContinueOffice(
    @field:SerializedName("success")
    val success: Boolean? = null
)