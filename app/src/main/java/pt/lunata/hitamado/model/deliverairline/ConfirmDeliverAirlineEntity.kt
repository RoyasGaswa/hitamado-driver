package pt.lunata.hitamado.model.deliverairline

data class ConfirmDeliverAirlineEntity(
    val success: Boolean? = null
)